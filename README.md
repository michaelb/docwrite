*NOTE:* this is not fully documented or tested.

Django DocWrite is a system for keeping data in the browser cache / upstream
cache, while selectively updating parts of the site with live data.

Its goal is twofold:

1. To allow use of  client-side logic on page-load to determine whether or not
   to punch through a full-page cache (such as an upstream cache)  and fetch
   fresh content for designated regions of the page (e.g. 

2.  To allow templatetag-based caching to be more natural to Django developers: Instead
    of "cache this and this" (what the cache templatetag does), its "cache
    everything, except for this and this". For certain sites, the latter makes
    a lot more sense (e.g., don't cache a user-specific menu, but cache the
    rest of the page).

Django DocWrite's default behavior uses inline JavaScript's document.write to
inject all non-cached HTML while the DOM is being constructed, so that a page
can be a mix of upstream cached data and freshly fetched blocks of HTML. Django
DocWrite's asyncrhonous mode allows for insertion of data with jQuery or
innerHTML (to replace some sort of loader spinner).

FAQ
-----


Should I use this?
======

Do you want full-page caching, or want to more aggressively use upstream
caching or caching on a reverse-proxy?  If not, then the answer is: No.
DocWrite doesn't do very much special that caching systems bundled.

If you do use caching at a reverse proxy level, or a CDN service like
CloudFlare, then the answer is: Maybe. The goal of DocWrite is to give you more
control over when a request reaches your Django app servers, while letting you
be as aggressive as possible with caching. For example, you can configure
CloudFlare to cache certain heavily-trafficked pages with a very long timeout
regardless of headers / cookies / session info, and then use DocWrite to
selectively punch through that cache to update an Avatar / user-menu for every
user, without the rest of the page being fetched. Or, you can rely on more
fully on the browser cache, and send headers indicating really long cache time
for certain pages, with the relevant, fresh content being fetched independently.

All that said, I'm still not sure if DocWrite is very useful for many cases,
need to do more testing and documenting.


Isn't document.write considered harmful?
======

It is. But I think this is a valid use-case, since `document.write` allows for
any sort of HTML fragment to be quickly inserted in the document during page
load.

Note that when `ASYNC = True`, DocWrite won't use `document.write` to insert
page content, since it doesn't make any sense.


Installation
-------

1. Add to settings.py

2. Include `docwrite.urls` into project urls

Usage
-----

### Example 1

    {% load docwrite %}
 
    <head>
    {% docwrite_head %}
    </head>

    <body>
    <div>Content...</div> <div>Content...</div> <div>Content...</div>

    {% docwrite messages %}
        {% if new_messages %}
            You have {{ new_messages }} unread messages!
        {% else  %}
            You have no new messages.
        {% endif %}
    {% enddocwrite %}

    {% docwrite profile_pic %} <img src="{{ user.pic.url }}" /> {% enddocwrite %}

    {% if not request.docwrite %}
        {# Only execute this the first time the page is being loaded #}
        {% for book in new_books %}
            {% render_book book %}
        {% endfor %}
    {% endif %}

    </body>

Will generate a page something like...:

    <head>
        <script src="docwrite/head/content.js?url=some/path"></script>
        <!-- which might contain something like:
            window.DOCWRITE = {};
            window.DOCWRITE.new_messages = "You have 3 new messages.";
            window.DOCWRITE.profile_pic = "<img src="me.jpg" />";
        -->
    </head>

    <body>
        <div>Content...</div> <div>Content...</div> <div>Content...</div>

        <script>document.write(window.DOCWRITE.new_messages || "Please log in for messages")</script>
        <script>document.write(window.DOCWRITE.profile_pic || "Please login for pic")</script>

        <div><img src="book1.jpg" />Example new book content 1</div>
        <div><img src="book2.jpg" />Example new book content 2</div>
        <div><img src="book3.jpg" />Example new book content 3</div>

    </body>


This page can then be cached upstream from Django, and it will only populate
the necessary portions (and won't run needless SQL queries for the books, in
this example).

If you use the `DOCWRITE_LOGIC` flag, we can actually conditionally include the
content.js based on arbitrary JavaScript logic. Setting `DOCWRITE_LOGIC` to the
precise value of `True`, for example, will use the default logic, which is
checking for the existence of a session cookie. This allows for page views from
logged-out users, etc, to never hit the Django app servers at all.


### Example 2


settings.py:

    DOCWRITE_ASYNC = True
    DOCWRITE_ASYNC_LOADING_HTML = '<img src="spinner.gif" />'


in a template:

    {# ... same as above example, but don't use docwrite_head, instead at the very end have: #}

    {% docwrite_footer %}
    </body>


The result of this will use cause content.js to be loaded after page load,
using the given HTML as a placeholder until it is loaded. This allows for a
faster user experience: the first request will hit the cache,


Contributing
---------


Help, feedback, or even pull requests are welcome of course :)


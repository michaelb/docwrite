from setuptools import setup
from os import path

try:
    README = open(path.join(
        path.dirname(__file__), "README.md")).read()
except IOError:
    README = ''

setup(name='docwrite',
        version='0.0.1',
        description='Caching trick to keep entire pages in upstream cache.',
        long_description=README,
        url='http://bitbucket.com/michaelb/docwrite/',
        author='michaelb',
        author_email='michaelpb@gmail.com',
        license='GPL 3.0',
        include_package_true=True,
        #entry_points = { 'console_scripts': [], },
        packages=['docwrite'],
        package_dir={ 'docwrite': 'docwrite' },
        #package_data={ 'docwrite': ['static/*'] },
        zip_safe=False)


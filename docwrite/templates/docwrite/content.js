{% comment %}
Main content file, serves all necessary data to the document.
{% endcomment %}

if (!window.DOCWRITE) { window.DOCWRITE = {}; }

{% if redirect %}
    window.DOCWRITE_REDIRECT = "{{ redirect|escapejs }}";
{% else %}
    {% for key, html in blocks %}
        window.DOCWRITE["{{ key|escapejs }}"] = "{{ html|escapejs }}";
    {% endfor %}
{% endif %}

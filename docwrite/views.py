from urlparse import urlparse

from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import resolve, reverse
from django.http import HttpResponseRedirect, Http404
from django.template.loader import render_to_string


from . import util

def _response_to_context(response):

    if response.streaming:
        # Is a streaming response, unsupported
        raise util.DocWriteViewException(
                "streaming responses re not supported")

    # All wrapped blocks
    wrapped_blocks = util.split_by_tokens(response.content)

    blocks = {}

    for metadata, actual_content in wrapped_blocks:

        # for now its just the name of the block, later we might want to encode
        # more data here
        block_name = metadata

        if not block_name or not util.check_block_name(block_name):
            raise util.DocWriteViewException("Invalid block markers in a "
                    "view not configured to use DocWrite response. "
                    "(Hint: Is there unescaped binary/non-HTML data in "
                    "the response?")

        blocks[block_name] = actual_content

    ctx = {
            'blocks': blocks.items(),
            'redirect': False,
        }

    return ctx



def contentjs(request):
    """
    The view that returns the "content.js" script containing the information
    necessary to reconstruct the page with document.writes in the front end.
    """
    s = util.get_settings()
    block_names = request.GET.get('blocks', u'').split(u',')

    # Get full URL & parse out path
    url = request.GET['url']
    parsed_result = urlparse(url)
    path = parsed_result.path

    # Resolve to the current view
    try:
        view, view_args, view_kwargs = resolve(path)
    except Http404:
        raise DocWriteException("Http404 for '%s'" % path)

    # Check properties about the URL --- did we mark it with docwrite options?
    if hasattr(view, "docwrite_options"):
        # Something like this: (TODO unfinished)
        ctx = view.docwrite_options.get_context()
    else:

        # Nope, use default, brutal behavior

        # Attach DocWriteObject to request
        request.docwrite = util.DocWriteObject(block_names)

        # Use our request, slightly tweaked
        # TODO monkey patch request to fake proper URL, GET parameters, etc
        view_kwargs['request'] = request

        try:
            response = view(*view_args, **view_kwargs)

        # TODO: handle different content types, redirections, exceptions
        except Http404:
            # Handle 404 Exception... hmmm
            pass

        else:
            # Generate context from typical response
            ctx = _response_to_context(response)

    # Generate the resulting JS
    rendered = render_to_string('docwrite/content.js', ctx)

    # Not using "render" shortcut since we don't want to accidentally trigger
    # context_processors, etc, again
    return HttpResponse(rendered, content_type="text/javascript")



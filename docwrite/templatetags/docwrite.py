# python
import json
import random

# django
from django.conf import settings
from django.utils.translation import ugettext as _
from django.utils.html import conditional_escape, escape
from django.utils.safestring import mark_safe
from django.middleware.csrf import get_token
from django import template
from django.core.exceptions import ImproperlyConfigured
from django.template.loader import render_to_string, get_template
from django.template import Template, Context

from django.core.urlresolvers import reverse


# import util class
from .. import util


# What to check for: settings.SESSION_COOKIE_NAME

register = template.Library()


DEFAULT_LOGIC = ('document.cookie.match(/^(.*;)?%s=[^;]+(.*)?$/)'
                    % settings.SESSION_COOKIE_NAME)


def _build_context(**kwargs):
    s = util.get_settings()
    c = {}
    if s['LOGIC'] == True:
        c['logic'] = DEFAULT_LOGIC
    elif s['LOGIC']:
        c['logic'] = s['LOGIC']
    else:
        c['logic'] = None

    c['use_async'] = bool(s['ASYNC'])
    c['async_use_jquery'] = bool(s['ASYNC_USE_JQUERY'])
    c['loading_block_contents'] = s['ASYNC_LOADING_HTML']

    c.update(kwargs)

    return c

HEAD_TAG_TEMPLATE = "docwrite/tag.html"


def _generate_script_tags(should_be_async, parser, token):
    """
    This outputs necessary script tags for DocWrite
    """
    s = util.get_settings()

    if bool(s['ASYNC']) != should_be_async:
        correct = "footer" if should_be_async else "header"
        incorrect = "header" if should_be_async else "footer"
        raise ImproperlyConfigured(("Configured to use docwrite_%s, "+
                "but found instead docwrite_%s") % (correct, incorrect))

    contents = token.split_contents()

    # pop off "docwrite_head"
    contents.pop(0)

    # Remaining "contents" is assumed to be block names to be passed to the
    # request
    block_names = ",".join(filter(bool, contents))

    script_url = reverse("docwrite.views.contentjs")

    ctx = _build_context(script_url=script_url, block_names=block_names)

    rendered = render_to_string(HEAD_TAG_TEMPLATE, ctx)

    return template.TextNode(rendered)


@register.tag(name="docwrite_head")
def docwrite_head(parser, token):
    return _generate_script_tags(False, parser, token)


@register.tag(name="docwrite_footer")
def docwrite_head(parser, token):
    return _generate_script_tags(True, parser, token)


class DocWriteBlockNode(template.Node):
    TEMPLATE_PATH = "docwrite/block.html"
    def __init__(self, text, block_name):
        # Precompile template
        self.inner_template = Template(text)
        self.block_template = get_template(self.TEMPLATE_PATH)
        self.block_name = block_name


    def render(self, context):
        request = context.get('request')

        if not request:
            raise ImproperlyConfigured("docwrite: 'request' not found in "
                    "context, should add using context_processor, or "
                    "properly using render() shortcut")

        # Render the innards correctly
        contents = self.inner_template.render(context)

        if hasattr(request, "docwrite"):

            # If its a DocWrite JS call, then lets do the contents
            result = util.wrapped_with_tokens(self.block_name, contents)
        else:
            # Not a DocWrite call, this means its the initial call, use our template
            ctx = Context(_build_context(block_name=self.block_name,
                                default_block_contents=contents))
            result = self.block_template.render(ctx)

        return mark_safe(result)


@register.tag(name="docwrite")
def docwrite_block(parser, token):
    """
    This outputs JS for a DocWrite block
    """
    contents = token.split_contents()

    # pop off "docwrite_head"
    contents.pop(0)

    # Get the name
    block_name = contents.pop(0)

    if not block_name or not util.check_block_name(block_name):
        raise util.DocWriteException("Invalid block name '%s'" % block_name)

    text = []

    # Thanks to http://code.djangoproject.com/ticket/16318
    while True:
        token = parser.tokens.pop(0)
        if token.contents == 'enddocwrite':
            break
        if token.token_type == template.TOKEN_VAR:
            text.append('{{')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('{%')
        text.append(token.contents)
        if token.token_type == template.TOKEN_VAR:
            text.append('}}')
        elif token.token_type == template.TOKEN_BLOCK:
            text.append('%}')

    return DocWriteBlockNode(''.join(text), block_name)


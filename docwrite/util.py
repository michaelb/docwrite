#!/usr/bin/env python

import os
import sys
import re
import subprocess
import optparse
from datetime import date

from django.conf import settings
"""
One cool trick that sysadmins don't want you to know!
"""
__author__ = "michaelb"
__copyright__ = "Copyright 2013"
__credits__ = ["michaelb"]
__license__ = "GPL"
__version__ = "0.0.1"
__email__ = "michaelpb@gmail.com"



# should be random enough ;)
SUFFICIENTLY_RANDOM_STRING = (u'x!2.TP@0]"9XohgviyRdYrs^c<qJ%9/yZA`x9R4)&BKe#qijWXjVo3a6u'
                    +u'+1re%rCTX`,A@x.>T7\'W1uF`#IcjIIHrY)v%d(ZDxi!')

SRS_TOKEN = u"\n<!--%s-->\n" % SUFFICIENTLY_RANDOM_STRING 

#SRS_OPEN = u"\n{{%s\n" % SUFFICIENTLY_RANDOM_STRING
#SRS_CLOSE = u"\n%s}}\n" % SUFFICIENTLY_RANDOM_STRING


def wrapped_with_tokens(metadata, html):
    """
    This a hacky thing used to rapidly parse HTTP response content when no meta
    information is given for a view, yet docwrite is still being used on it.
    """
    return "%s%s\n%s%s" % (SRS_TOKEN, metadata, html, SRS_TOKEN)

def split_by_tokens(content):
    """
    Split by delimiter token
    """
    content = unicode(content)
    split_contents = content.split(SRS_TOKEN)
    odd = split_contents[1::2]
    # Split and get out odd (wrapped) content
    result = []
    for wrapped_content in odd:
        metadata, sep, actual_content = wrapped_content.partition("\n")
        result.append((metadata, actual_content))

    return result


VALID_BLOCK_NAME_RE = re.compile(r'^[\w-]+$')
def check_block_name(block_name):
    return VALID_BLOCK_NAME_RE.match(block_name)

_settings_dict = None
def get_settings():
    global _settings_dict
    if _settings_dict:
        return _settings_dict

    settings_dict = {}
    def setup(key, default):
        if hasattr(settings, "DOCWRITE"):
            settings_dict[key] = settings.DOCWRITE.get(key, default)
        else:
            settings_dict[key] = getattr(settings,
                    "DOCWRITE_%s" % key, default)

    setup("LOGIC", False)
    setup("ASYNC", False)
    #setup("ASYNC", True)
    setup("ASYNC_LOADING_HTML", None)
    setup("ASYNC_USE_JQUERY", False)
    _settings_dict = settings_dict
    return settings_dict

class DocWriteViewOptions(dict):
    def __init__(self, view, **kwargs):
        self.view = view
        super(dict, self).__init__(kwargs)

    def get_context(self):
        # TODO need to figure out what to do here
        raise Exception("not implemented")


class DocWriteObject(object):
    def __init__(self, blocks):
        self.blocks = blocks

    def __in__(self, e):
        return e in self.blocks

    def __iter__(self):
        return iter(self.blocks)

class DocWriteException(Exception): pass
class DocWriteViewException(DocWriteException): pass



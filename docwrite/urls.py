from django.conf.urls import patterns, include, url

urlpatterns = patterns('docwrite.views',
    # Main JS that loads page content via JS, typically in <head> tag
    (r'^head/content.js$', 'contentjs'),
)

